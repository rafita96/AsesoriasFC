function seleccionar(cita_id, fecha_id, hora_id){
    $("#id_input").val(cita_id);
    $("#fecha_input").val(fecha_id);
    $("#hora_input").val(hora_id);

    if(seleccionado != null){
        $(seleccionado).addClass('bg-warning');
        $(seleccionado).removeClass('bg-hithere');
    }
    seleccionado = "#" + cita_id + "_" + fecha_id + "_" + hora_id;
    $(seleccionado).addClass('bg-hithere');
    $(seleccionado).removeClass('bg-warning');
}

function enviar(){
    if($("#id_input").val() != "" || $("#fecha_input").val() != "" || $("#hora_input").val() != ""){
        $("#aceptar_form").submit();
    }else{
        toastr("No has elegido una hora.");
    }
}

var seleccionado = null;
$('.modal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    if(button.data('aceptar') == '1'){
        $("#id_input").val("");
        $("#fecha_input").val("");
        $("#hora_input").val("");

        if(seleccionado != null){
            $(seleccionado).addClass('bg-warning');
            $(seleccionado).removeClass('bg-hithere');
        }
        seleccionado = null;
    }
});