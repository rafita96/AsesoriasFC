<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Form\FechaType;


class CitaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tema')
                ->add('materia')
                ->add('cantidad', null, array(
                        'label' => 'Cantidad de alumnos',
                        'attr' => array(
                            "value" => 1,
                            'min' => 1
                        ),
                    )
                )
                ->add('horario', CollectionType::class, array(
                    'entry_type' => FechaType::class,
                    'label' => false,
                    'entry_options' => array('label' => false),
                    'allow_add' => true,
                ));
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Cita'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_cita';
    }


}
