<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Fecha
 *
 * @ORM\Table(name="fecha")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FechaRepository")
 */
class Fecha
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;


    /**
     * @ORM\ManyToMany(targetEntity="Horario", cascade={"persist"})
     * @ORM\JoinTable(name="fecha_horarios",
     *      joinColumns={@ORM\JoinColumn(name="fecha_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="horario_id", referencedColumnName="id")}
     *      )
     */
    private $horarios;

    /**
     * @ORM\ManyToOne(targetEntity="Cita", inversedBy="horario")
     * @ORM\JoinColumn(name="cita", referencedColumnName="id")
     */
    private $cita;

    public function __construct() {
        $this->horarios = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Add horario
     *
     * @param \AppBundle\Entity\Horario $horario
     *
     * @return Fecha
     */
    public function addHorario(\AppBundle\Entity\Horario $horario)
    {
        $this->horarios[] = $horario;

        return $this;
    }

    /**
     * Remove horario
     *
     * @param \AppBundle\Entity\Horario $horario
     */
    public function removeHorario(\AppBundle\Entity\Horario $horario)
    {
        $this->horarios->removeElement($horario);
    }

    /**
     * Get horarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHorarios()
    {
        return $this->horarios;
    }

    /**
     * Set cita
     *
     * @param \AppBundle\Entity\Cita $cita
     *
     * @return Fecha
     */
    public function setCita(\AppBundle\Entity\Cita $cita = null)
    {
        $this->cita = $cita;

        return $this;
    }

    /**
     * Get cita
     *
     * @return \AppBundle\Entity\Cita
     */
    public function getCita()
    {
        return $this->cita;
    }

}
