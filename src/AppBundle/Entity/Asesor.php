<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Asesor
 *
 * @ORM\Table(name="asesor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AsesorRepository")
 */
class Asesor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="asesor")
     * @ORM\JoinTable(name="user")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Cita", mappedBy="asesor", cascade={"persist"})
     */
    private $citas;

    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="Administrador", mappedBy="asesores")
     */
    private $administradores;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->citas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Asesor
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add cita
     *
     * @param \AppBundle\Entity\Cita $cita
     *
     * @return Asesor
     */
    public function addCita(\AppBundle\Entity\Cita $cita)
    {
        $this->citas[] = $cita;

        return $this;
    }

    /**
     * Remove cita
     *
     * @param \AppBundle\Entity\Cita $cita
     */
    public function removeCita(\AppBundle\Entity\Cita $cita)
    {
        $this->citas->removeElement($cita);
    }

    /**
     * Get citas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCitas()
    {
        return $this->citas;
    }

    /**
     * Add administradore
     *
     * @param \AppBundle\Entity\Administrador $administradore
     *
     * @return Asesor
     */
    public function addAdministradore(\AppBundle\Entity\Administrador $administradore)
    {
        $this->administradores[] = $administradore;

        return $this;
    }

    /**
     * Remove administradore
     *
     * @param \AppBundle\Entity\Administrador $administradore
     */
    public function removeAdministradore(\AppBundle\Entity\Administrador $administradore)
    {
        $this->administradores->removeElement($administradore);
    }

    /**
     * Get administradores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdministradores()
    {
        return $this->administradores;
    }
}
