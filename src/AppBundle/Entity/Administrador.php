<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Administrador
 *
 * @ORM\Table(name="administrador")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdministradorRepository")
 */
class Administrador
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="administrador", cascade={"persist"})
     * @ORM\JoinTable(name="user")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Asesor", inversedBy="administradores")
     * @ORM\JoinTable(name="asesores_administradores")
     */
    private $asesores;


    public function __construct() {
        $this->asesores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add asesore
     *
     * @param \AppBundle\Entity\Asesor $asesore
     *
     * @return Administrador
     */
    public function addAsesore(\AppBundle\Entity\Asesor $asesore)
    {
        $this->asesores[] = $asesore;

        return $this;
    }

    /**
     * Remove asesore
     *
     * @param \AppBundle\Entity\Asesor $asesore
     */
    public function removeAsesore(\AppBundle\Entity\Asesor $asesore)
    {
        $this->asesores->removeElement($asesore);
    }

    /**
     * Get asesores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAsesores()
    {
        return $this->asesores;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Administrador
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
