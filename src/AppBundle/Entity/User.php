<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="`user`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string")
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="aPaterno", type="string")
     */
    private $aPaterno;

    /**
     * @var string
     *
     * @ORM\Column(name="aMaterno", type="string")
     */
    private $aMaterno;

    /**
     * @ORM\OneToOne(targetEntity="Alumno", mappedBy="user", cascade={"persist"})
     */
    private $alumno;

    /**
     * @ORM\OneToOne(targetEntity="Asesor", mappedBy="user", cascade={"persist"})
     */
    private $asesor;

    /**
     * @ORM\OneToOne(targetEntity="Administrador", mappedBy="user", cascade={"persist"})
     */
    private $administrador;

    /**
     * @ORM\OneToOne(targetEntity="Token", mappedBy="user")
     */
    private $inviteToken;

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set aPaterno
     *
     * @param string $aPaterno
     *
     * @return User
     */
    public function setAPaterno($aPaterno)
    {
        $this->aPaterno = $aPaterno;

        return $this;
    }

    /**
     * Get aPaterno
     *
     * @return string
     */
    public function getAPaterno()
    {
        return $this->aPaterno;
    }

    /**
     * Set aMaterno
     *
     * @param string $aMaterno
     *
     * @return User
     */
    public function setAMaterno($aMaterno)
    {
        $this->aMaterno = $aMaterno;

        return $this;
    }

    /**
     * Get aMaterno
     *
     * @return string
     */
    public function getAMaterno()
    {
        return $this->aMaterno;
    }

    /**
     * Set matricula
     *
     * @param string $matricula
     *
     * @return User
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;

        return $this;
    }

    /**
     * Get matricula
     *
     * @return string
     */
    public function getMatricula()
    {
        return $this->matricula;
    }

    /**
     * Set alumno
     *
     * @param \AppBundle\Entity\Alumno $alumno
     *
     * @return User
     */
    public function setAlumno(\AppBundle\Entity\Alumno $alumno = null)
    {
        $this->alumno = $alumno;

        return $this;
    }

    /**
     * Get alumno
     *
     * @return \AppBundle\Entity\Alumno
     */
    public function getAlumno()
    {
        return $this->alumno;
    }

    /**
     * Set asesor
     *
     * @param \AppBundle\Entity\Asesor $asesor
     *
     * @return User
     */
    public function setAsesor(\AppBundle\Entity\Asesor $asesor = null)
    {
        $this->asesor = $asesor;

        return $this;
    }

    /**
     * Get asesor
     *
     * @return \AppBundle\Entity\Asesor
     */
    public function getAsesor()
    {
        return $this->asesor;
    }

    /**
     * Set administrador
     *
     * @param \AppBundle\Entity\Administrador $administrador
     *
     * @return User
     */
    public function setAdministrador(\AppBundle\Entity\Administrador $administrador = null)
    {
        $this->administrador = $administrador;

        return $this;
    }

    /**
     * Get administrador
     *
     * @return \AppBundle\Entity\Administrador
     */
    public function getAdministrador()
    {
        return $this->administrador;
    }

    /**
     * Set inviteToken
     *
     * @param \AppBundle\Entity\Token $inviteToken
     *
     * @return User
     */
    public function setInviteToken(\AppBundle\Entity\Token $inviteToken = null)
    {
        $this->inviteToken = $inviteToken;

        return $this;
    }

    /**
     * Get inviteToken
     *
     * @return \AppBundle\Entity\Token
     */
    public function getInviteToken()
    {
        return $this->inviteToken;
    }
}
