<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Entity\Horario;

use Doctrine\ORM\EntityManagerInterface;

/**
* Comando que inicializa la base de datos
*/
class AppCommand extends Command
{

    private $em;

    // Se le inyecta el controlador de entidades para que pueda insertar en la base
    // de datos
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->em = $entityManager;
    }

    protected function configure()
    {
        $this
        // El nombre del comando (la parte después de "bin/console")
        ->setName('app:init')

        // La descripción sobre el comando, lo que se muestra al ejecutar "php bin/console list"
        ->setDescription('Inicializa la base de datos')

        // Descripción completa al poner la opción: "--help"
        ->setHelp('Este comando mete valores a la base de datos')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $cmd = $this->em->getClassMetadata('AppBundle:Horario');
        $connection = $this->em->getConnection();
        $connection->beginTransaction();

        try {
            $connection->query('SET FOREIGN_KEY_CHECKS=0');
            // Limpia el contenido de las tablas
            $connection->query('DELETE FROM '.$cmd->getTableName());
            // Inicializa el contador de auto incremento
            $connection->query('ALTER TABLE '.$cmd->getTableName().' AUTO_INCREMENT = 1');
            $connection->query('SET FOREIGN_KEY_CHECKS=1');
            // Ejecuta los querys
            $connection->commit();
            $output->writeln('Se limpió la base de datos.');
        } catch (\Exception $e) {
            $connection->rollback();
            $output->writeln('Hubo un error:'.$e);
        }

        // Inserta 10 horas en la tabla de horarios, inicia a las 8:00 AM
        // y termina a las 6:00 PM
        // por ejemplo las 8:00 AM será 80000
        // Se hizo de esta forma porque solamente hay horas fijas en el sistema.
        for($i = 0; $i < 10; $i++){
            $hora = new Horario();

            $hora->setHora(($i+8)*10000);

            $this->em->persist($hora);
        }

        // Ejecuta los cambios en la base de datos
        $this->em->flush();
        $output->writeln('Listo.');
    }
}