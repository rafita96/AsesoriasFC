<?php

namespace AppBundle\Command;

use FOS\UserBundle\Command\CreateUserCommand as BaseCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

use AppBundle\Tools\UserManipulator;

/** 
* Crea un super administrador 
*/
class CreateUserCommand extends BaseCommand
{
    private $userManipulator;

    // Inyecta el modelo del usuario que maneja FOSUserBundle
    public function __construct(UserManipulator $userManipulator)
    {
        parent::__construct($userManipulator);

        $this->userManipulator = $userManipulator;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        // Pide los datos de entrada para crear el superadministrador
        $this
            ->setName('fos:user:create')
            ->setDescription('Create a user.')
            ->setDefinition(array(
                new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                new InputArgument('email', InputArgument::REQUIRED, 'The email'),
                new InputArgument('password', InputArgument::REQUIRED, 'The password'),
                new InputArgument('nombre', InputArgument::REQUIRED, 'The nombre'),
                new InputArgument('aPaterno', InputArgument::REQUIRED, 'The aPaterno'),
                new InputArgument('aMaterno', InputArgument::REQUIRED, 'The aMaterno'),
                new InputOption('super-admin', null, InputOption::VALUE_NONE, 'Set the user as super admin'),
                new InputOption('inactive', null, InputOption::VALUE_NONE, 'Set the user as inactive'),
            ))
            ->setHelp(<<<'EOT'
The <info>fos:user:create</info> command creates a user:

  <info>php %command.full_name% matthieu</info>

This interactive shell will ask you for an email and then a password.

You can alternatively specify the email and password as the second and third arguments:

  <info>php %command.full_name% matthieu matthieu@example.com mypassword</info>

You can create a super admin via the super-admin flag:

  <info>php %command.full_name% admin --super-admin</info>

You can create an inactive user (will not be able to log in):

  <info>php %command.full_name% thibault --inactive</info>

EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $nombre = $input->getArgument('nombre');
        $aMaterno = $input->getArgument('aMaterno');
        $aPaterno = $input->getArgument('aPaterno');
        $inactive = $input->getOption('inactive');
        $superadmin = $input->getOption('super-admin');

        $this->userManipulator->create($username, $password, $email, $nombre, $aMaterno, $aPaterno, !$inactive, $superadmin);

        $output->writeln(sprintf('Created user <comment>%s</comment>', $username));
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();

        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username:');
            $question->setValidator(function ($username) {
                if (empty($username)) {
                    throw new \Exception('Username can not be empty');
                }

                return $username;
            });
            $questions['username'] = $question;
        }

        if (!$input->getArgument('email')) {
            $question = new Question('Please choose an email:');
            $question->setValidator(function ($email) {
                if (empty($email)) {
                    throw new \Exception('Email can not be empty');
                }

                return $email;
            });
            $questions['email'] = $question;
        }

        if (!$input->getArgument('password')) {
            $question = new Question('Please choose a password:');
            $question->setValidator(function ($password) {
                if (empty($password)) {
                    throw new \Exception('Password can not be empty');
                }

                return $password;
            });
            $question->setHidden(true);
            $questions['password'] = $question;
        }

        if (!$input->getArgument('nombre')) {
            $question = new Question('Please choose an nombre:');
            $question->setValidator(function ($nombre) {
                if (empty($nombre)) {
                    throw new \Exception('nombre can not be empty');
                }

                return $nombre;
            });
            $questions['nombre'] = $question;
        }

        if (!$input->getArgument('aPaterno')) {
            $question = new Question('Please choose an aPaterno:');
            $question->setValidator(function ($aPaterno) {
                if (empty($aPaterno)) {
                    throw new \Exception('aPaterno can not be empty');
                }

                return $aPaterno;
            });
            $questions['aPaterno'] = $question;
        }

        if (!$input->getArgument('aMaterno')) {
            $question = new Question('Please choose an aMaterno:');
            $question->setValidator(function ($aMaterno) {
                if (empty($aMaterno)) {
                    throw new \Exception('aMaterno can not be empty');
                }

                return $aMaterno;
            });
            $questions['aMaterno'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }
}
