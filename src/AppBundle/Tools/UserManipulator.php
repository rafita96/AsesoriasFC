<?php 
namespace AppBundle\Tools;


use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Util\UserManipulator as Manipulator;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use AppBundle\Entity\Administrador;
use AppBundle\Entity\Asesor;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class UserManipulator extends Manipulator
{
    /**
     * User manager.
     *
     * @var UserManagerInterface
     */
    private $userManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    private $em;

    /**
     * UserManipulator constructor.
     *
     * @param UserManagerInterface     $userManager
     * @param EventDispatcherInterface $dispatcher
     * @param RequestStack             $requestStack
     */
    public function __construct(UserManagerInterface $userManager, EventDispatcherInterface $dispatcher, RequestStack $requestStack, EntityManagerInterface $entityManager)
    {

        parent::__construct($userManager, $dispatcher, $requestStack);

        $this->userManager = $userManager;
        $this->dispatcher = $dispatcher;
        $this->requestStack = $requestStack;

        $this->em = $entityManager;
    }

    /**
     * Creates a user and returns it.
     *
     * @param string $username
     * @param string $password
     * @param string $email
     * @param bool   $active
     * @param bool   $superadmin
     *
     * @return \FOS\UserBundle\Model\UserInterface
     */
    public function create($username, $password, $email, $nombre, $aMaterno, $aPaterno,$active, $superadmin)
    {
        $repository = $this->em->getRepository(User::class);
        $user = $repository->findOneByUsername($username);
        if($user == null){
            $user = $this->userManager->createUser();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPlainPassword($password);
            $user->setNombre($nombre);
            $user->setAPaterno($aPaterno);
            $user->setAMaterno($aMaterno);
        }else{
            $user->removeRole();
        }

        $actualizar = false;
        // $user->addRole('ROLE_ADMIN');
        if($user->getAdministrador() == null){
            $administrador = new Administrador();
            $administrador->setUser($user);
            $user->setAdministrador($administrador);
            $this->em->persist($administrador);
            $actualizar = true;
        }

        if($user->getAsesor() == null){
            $asesor = new Asesor();
            $asesor->setUser($user);
            $user->setAsesor($asesor);
            $this->em->persist($asesor);
            $actualizar = true;
        }

        if($actualizar){
            $this->em->flush();
        }

        $user->setEnabled((bool) $active);
        $user->setSuperAdmin(true);

        $this->userManager->updateUser($user);

        $event = new UserEvent($user, $this->getRequest());
        $this->dispatcher->dispatch(FOSUserEvents::USER_CREATED, $event);

        return $user;
    }

    /**
     * @return Request
     */
    private function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }
}
