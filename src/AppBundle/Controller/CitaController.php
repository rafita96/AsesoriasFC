<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Cita;
use AppBundle\Entity\Horario;
use AppBundle\Entity\Fecha;
use AppBundle\Form\CitaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CitaController extends Controller
{
    /**
     * @Route("/solicitud/new", name="citas_new")
     */
    public function newAction(Request $request)
    {
        $roles = $this->getUser()->getRoles();
        if(in_array("ROLE_ADMIN", $roles) || in_array("ROLE_SUPER_ADMIN", $roles) ||
            $this->getUser()->getAsesor()){
            return $this->redirectToRoute('citas');
        }

        $alumno = $this->getUser()->getAlumno();

        $cita = new Cita();

        date_default_timezone_set('America/Tijuana');
        $hoy = new \DateTime();
        // Lunes=0 ... Viernes=4
        $dia = intval($hoy->format('w')) - 1;
        // return new Response($dia);
        if($dia == -1){
            $dia = 6;
        }
        for($i = 0; $i < 7; $i++){

            // Si no es sabado ni domingo
            if(($dia+$i)%7 != 5 && ($dia+$i)%7 != 6){
                $otro_dia =new \DateTime();
                $otro_dia->modify("+".$i." days");

                $fecha = new Fecha();
                $fecha->setFecha($otro_dia);
                $cita->addHorario($fecha);
            } 
        }

        $form = $this->createForm(CitaType::class, $cita);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $cita->setAlumno($alumno);
            $cita->setEstado($cita->PENDIENTE);

            $em = $this->getDoctrine()->getManager();
            $quitar = array();
            foreach ($cita->getHorario() as $fecha) {
                if(count($fecha->getHorarios()) > 0){
                    $fecha->setCita($cita);
                    $em->persist($fecha);
                }else{
                    array_push($quitar, $fecha);
                }
            }

            foreach ($quitar as $fecha) {
                $cita->removeHorario($fecha);
            }
            


            // Debemos encontrar cuantos dias faltan para llegar al ultimo dia seleccionado
            $max = null;
            foreach ($cita->getHorario() as $fecha) {
                if($max == null){
                    $max = $fecha;
                }else if($fecha->getFecha() > $max->getFecha()){
                    $max = $fecha;
                }
            }

            $horaMax = $max->getHorarios()[0];
            foreach ($max->getHorarios() as $hora) {
                if($hora->getHora() > $horaMax->getHora()){
                    $horaMax = $hora;
                }
            }

            $expiracion = clone $max->getFecha();
            $expiracion->setTime($horaMax->getHora()/10000,0,0);
            $cita->setExpiracion($expiracion);

            $em->persist($cita);
            $em->flush();

            $this->addFlash('success','Has creado una nueva solicitud.');

            return $this->redirectToRoute('solicitudes');
        }

        return $this->render('cita/new.html.twig', array(
            'form' => $form->createView(),
            'dia' => $dia
        ));
    }

    /**
     * @Route("/citas/", name="citas")
     *
     *
     */
    public function verAction(Request $request){
        $roles = $this->getUser()->getRoles();

        if(in_array("ROLE_SUPER_ADMIN", $roles) || in_array("ROLE_ADMIN", $roles)
            || in_array("ROLE_ASESOR", $roles)){
            $asesor = $this->getUser()->getAsesor();
            
            $repository = $this->getDoctrine()->getRepository(Cita::class);

            $citas = $repository->findByAsesor($asesor);
        }else{
            $citas = $this->getUser()->getAlumno()->getCitas();
        }

        $today = new \DateTime();
        $em = $this->getDoctrine()->getManager();
        $actualizar = false;
        foreach ($citas as $cita) {
            if($cita->getEstado() == 0 && $today > $cita->getExpiracion()){
                $cita->setEstado($cita->EXPIRADO);

                $em->persist($cita);
                if(!$actualizar){
                    $actualizar = true;
                }
            }else if($cita->getEstado() == $cita->CITADO && $today > $cita->getExpiracion()){
                $cita->setEstado($cita->FINALIZADO);

                $em->persist($cita);
                if(!$actualizar){
                    $actualizar = true;
                }
            }
        }

        if($actualizar){
            $em->flush();
        }

        // if(in_array("ROLE_ALUMNO", $roles)){
        //     return $this->render('cita/aceptadas.html.twig', array(
        //         'citas' => $citas,
        //     ));
        // }else{
        //     return $this->render('cita/lista.html.twig', array(
        //         'citas' => $citas,
        //     ));
        // }

        return $this->render('cita/aceptadas.html.twig', array(
            'citas' => $citas,
        ));
    }

    /**
     * @Route("/cita/eliminar/{id}", name="citas_eliminar")
     */
    public function eliminarAction(Request $request, $id){
        $alumno = $this->getUser();

        $repository = $this->getDoctrine()->getRepository(Cita::class);
        $cita = $repository->findOneById($id);
        if(!$cita){
            throw new NotFoundHttpException("La solicitud no existe.");
        }

        // $cita = $repository->findByAlumnoCita($alumno, $id);

        if($cita->getAsesor() != null){
            $this->addFlash('danger','Está solicitud no se puede eliminar.');
            return $this->redirectToRoute('solicitudes');
        }

        if($cita->getAlumno()->getUser()->getId() == $alumno->getId()){

            $em = $this->getDoctrine()->getManager();
            foreach ($cita->getHorario() as $fecha) {
                $cita->removeHorario($fecha);
                $em->remove($fecha);
            }

            $em->remove($cita);
            $em->flush();


            $this->addFlash('success','La solicitud se ha eliminado.');
            return $this->redirectToRoute('solicitudes');
        }
        
        throw new NotFoundHttpException("No eres el dueño de esta solicitud.");
    }

    /**
     * @Route("/solicitudes/", name="solicitudes")
     */
    public function solicitudesAction(Request $request){
        $roles = $this->getUser()->getRoles();
        if(in_array("ROLE_ADMIN", $roles) || in_array("ROLE_SUPER_ADMIN", $roles) ||
            $this->getUser()->getAsesor()) {
            
            $id_cita = $request->request->get('cita_id');
            if (!empty($id_cita)) {
                $em = $this->getDoctrine()->getManager();

                $cita = $this->getDoctrine()->getRepository(Cita::class)->find($id_cita);
                $asesor = $this->getUser();
                $cita->setAsesor($asesor);
                $cita->setEstado(true);

                $em->persist($cita);
                $em->flush();
            }
            $repository = $this->getDoctrine()->getRepository(Cita::class);
            date_default_timezone_set('America/Tijuana');
            $today = new \DateTime();
            $citas = $repository->findByDisponibles($today);

            return $this->render('cita/solicitudes.html.twig', array(
                'citas' => $citas,
            )); 
        }

        $citas = $this->getUser()->getAlumno()->getCitas();

        $today = new \DateTime();
        $em = $this->getDoctrine()->getManager();
        $actualizar = false;
        foreach ($citas as $cita) {
            if($cita->getEstado() == 0 && $today > $cita->getExpiracion()){
                $cita->setEstado($cita->EXPIRADO);

                $em->persist($cita);
                if(!$actualizar){
                    $actualizar = true;
                }
            }else if($cita->getEstado() == $cita->CITADO && $today > $cita->getExpiracion()){
                $cita->setEstado($cita->FINALIZADO);

                $em->persist($cita);
                if(!$actualizar){
                    $actualizar = true;
                }
            }
        }

        if($actualizar){
            $em->flush();
        }

        return $this->render('cita/lista.html.twig', array(
            'citas' => $citas,
        ));
    }

    /**
     * @Route("/solicitud/aceptar", name="solicitudes_aceptar")
     */
    public function aceptarAction(Request $request, \Swift_Mailer $mailer){
        $id = $request->request->get('id');
        $fecha_id = $request->request->get('fecha');
        $hora_id = $request->request->get('hora');

        $repository = $this->getDoctrine()->getRepository(Cita::class);
        $cita = $repository->findOneById($id);

        $roles = $this->getUser()->getRoles();
        if(in_array("ROLE_ALUMNO", $roles)){
            throw new NotFoundHttpException("Error, un alumno puede aceptar un solicitud de asesoría.");
        }

        if($cita->getAsesor()){
            throw new NotFoundHttpException("La cita fue agendada por otra persona.");
        }

        $fecha = null;
        foreach ($cita->getHorario() as $temp) {
            if($temp->getId() == $fecha_id){
                $fecha = $temp;
                break;
            }
        }

        if($fecha == null){
            throw new NotFoundHttpException("El horario solicitado no está disponible.");
        }

        $hora = null;
        foreach ($fecha->getHorarios() as $temp) {
            if($temp->getId() == $hora_id){
                $hora = $temp;
                break;
            }
        }

        if($hora == null){
            throw new NotFoundHttpException("El horario solicitado no está disponible.");
        }

        $date = clone $fecha->getFecha();
        $date->setTime($hora->getHora()/10000,0,0);
        $asesor = $this->getUser()->getAsesor();

        // Dos citas a la misma hora
        /*foreach ($asesor->getCitas() as $cita) {
            if($cita->getFecha() == $date){
                $this->addFlash('danger','No puedes tener 2 citas a la misma hora.');
                return $this->redirectToRoute('solicitudes');
            }
        }*/
        

        $cita->setFecha($date);

        $expiracion = clone $date;
        $expiracion->modify("+2 hours");
        $cita->setExpiracion($expiracion);
        $cita->setAsesor($asesor);
        $asesor->addCita($cita);


        $cita->setEstado($cita->CITADO);

        $em = $this->getDoctrine()->getManager();
        $em->persist($asesor);
        $em->persist($cita);
        $em->flush();

        $message = (new \Swift_Message('Tu solicitud ha sido aceptada.'))
        ->setFrom('asesoriasfc@noreply')
        ->setTo($cita->getAlumno()->getUser()->getEmail())
        ->setBody(
            $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                'email/aprobada.html.twig',
                array('asesor' => $this->getUser(),
                        'cita' => $cita)
            ),
            'text/html'
        )
        ;

        $mailer->send($message);

        $this->addFlash('success','Has aceptado una solicitud.');
        return $this->redirectToRoute('citas');
    }

    /**
     * @Route("/cita/historial", name="cita_historial")
     */
    public function historialAction(Request $request){
        $today = new \DateTime();
        $roles = $this->getUser()->getRoles();

        if(in_array("ROLE_SUPER_ADMIN", $roles) || in_array("ROLE_ADMIN", $roles)
            || in_array("ROLE_ASESOR", $roles)){
            $asesor = $this->getUser()->getAsesor();
            
            $repository = $this->getDoctrine()->getRepository(Cita::class);

            $temp = $repository->findByAsesor($asesor);
        }else{
            $temp = $this->getUser()->getAlumno()->getCitas();
        }
        $em = $this->getDoctrine()->getManager();
        $actualizar = false;
        foreach ($temp as $cita) {
            if($cita->getEstado() == 0 && $today > $cita->getExpiracion()){
                $cita->setEstado($cita->EXPIRADO);

                $em->persist($cita);
                if(!$actualizar){
                    $actualizar = true;
                }
            }else if($cita->getEstado() == $cita->CITADO && $today > $cita->getExpiracion()){
                $cita->setEstado($cita->FINALIZADO);

                $em->persist($cita);
                if(!$actualizar){
                    $actualizar = true;
                }
            }
        }

        if($actualizar){
            $em->flush();
        }

        $citas = array();
        foreach ($temp as $cita) {
            if($cita->getEstado() == $cita->FINALIZADO){
                array_push($citas, $cita);
            }
        }

        return $this->render('cita/historial.html.twig', array(
            'citas' => $citas,
        ));
    }
}
