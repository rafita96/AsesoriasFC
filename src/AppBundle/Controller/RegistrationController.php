<?php

namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\RegistrationController as BaseController;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\UserType;

use AppBundle\Entity\Alumno;

class RegistrationController extends BaseController
{
    public function registerAction(Request $request){
        /** @var $userManager UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);
        
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if(null !== $event->getResponse()){
            return $event->getResponse();
        }

        $form = $this->createForm(UserType::class, $user);
        // $form->setData($user);

        $form->handleRequest($request);

        if($form->isSubmitted()){
            if($form->isValid()){

                $user->setEmail($user->getUsername().'@uabc.edu.mx');
                $user->addRole('ROLE_ALUMNO');

                $encoder = $this->container->get("security.password_encoder");
                $encoded = $encoder->encodePassword($user, $form["password"]->getData());
                $user->setPassword($encoded);

                $matricula = $form["matricula"]->getData();

                $alumno = new Alumno();
                $alumno->setMatricula($matricula);
                $alumno->setUser($user);
                $user->setAlumno($alumno);

                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('fos_user_registration_confirmed');
                    $response = new RedirectResponse($url);
                }

                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
                $em = $this->getDoctrine()->getManager();
                $em->persist($alumno);
                $em->flush();

                return $response;
            }

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_FAILURE, $event);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }
        }

        return $this->render('@FOSUser/Registration/register.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
