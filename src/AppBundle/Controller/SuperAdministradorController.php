<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\User;
use AppBundle\Entity\Asesor;
use AppBundle\Entity\Administrador;
use AppBundle\Entity\Token;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SuperAdministradorController extends Controller
{
    /**
     * @Route("/super/admin/", name="administradores")
     */
    public function administradoresAction(Request $request, \Swift_Mailer $mailer){

        $defaultData = array('message' => 'Enviar solicitud de registro');
        $form = $this->createFormBuilder($defaultData)
            ->add('correo', TextType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            // data is an array with "name", "email", and "message" keys
            $correo = $form["correo"]->getData();

            $repository = $this->getDoctrine()->getRepository(User::class);
            $user = $repository->findOneByUsername($correo);
            if($user != null){
                if(in_array("ROLE_ADMIN", $user->getRoles()) || in_array('ROLE_SUPER_ADMIN', $user->getRoles())){
                    $this->addFlash('danger','El usuario con ese correo ya es un administrador.');
                    return $this->redirectToRoute('administradores');
                }else{
                    // $user->addRole('ROLE_ADMIN');
                    if($user->getAdministrador() == null){
                        $administrador = new Administrador();
                        $administrador->setUser($user);
                        $user->setAdministrador($administrador);
                        $em->persist($administrador);
                    }

                    if($user->getAsesor() == null){
                        $asesor = new Asesor();
                        $asesor->setUser($user);
                        $user->setAsesor($asesor);
                        $em->persist($asesor);
                    }
                    $user->removeRole("ROLE_ASESOR");
                    $user->removeRole("ROLE_ALUMNO");
                    $user->addRole("ROLE_ADMIN");
                    $user->setEnabled(true);

                    $em->persist($user);
                    $em->flush();
                    
                    $this->addFlash('success','El usuario con el correo '.$correo.'@uabc.edu.mx ahora es administrador.');
                }
            }else{
                $repository = $this->getDoctrine()->getRepository(Token::class);
                $token = $repository->findOneByCorreo($correo);
                if($token == null){
                    $token = new Token();
                    $token->setUser($this->getUser());
                    $random = md5(random_bytes(10));
                    $token->setToken($random);
                    $token->setCorreo($correo);

                    $em->persist($token);
                    $em->flush();
                }

                $message = (new \Swift_Message('Correo de confirmación'))
                    ->setFrom('asesoriasfc@noreply')
                    ->setTo($correo.'@uabc.edu.mx')
                    ->setBody(
                        $this->renderView(
                            // app/Resources/views/Emails/registration.html.twig
                            'email/registration_admin.email.twig',
                            array('token' => $token->getToken())
                        ),
                        'text/html'
                    );

                $mailer->send($message);
                $this->addFlash('success','Correo enviado exitosamente.');
            }
        }

        $em = $this->getDoctrine()->getEntityManager();
        $repository = $em->getRepository(User::class);
        $administradores = $repository->findByRole("ROLE_ADMIN");

        return $this->render('admin/administradores.html.twig', array(
            'administradores' => $administradores,
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/super/admin/{id}", name="look_admin")
     */
    public function lookAdminAction(Request $request, $id)
    {
        $repository = $this->getDoctrine()->getManager()->getRepository(User::class);
        $admin = $repository->findOneById($id);
        
        if($admin == null){
            throw new NotFoundHttpException('No se encontró el usuario que estas buscando.');
        }

        if($admin->getAdministrador() == null){
            throw new NotFoundHttpException('El usuario que buscas no es un administrador.');
        }

        $asesores = $admin->getAdministrador()->getAsesores();

        return $this->render('admin/home.html.twig', array(
            'asesores' => $asesores,
            'admin' => $admin,
        ));
    }

    /**
     * @Route("/super/admin/delete/{id}", name="delete_admin")
     */
    public function deleteAdminAction(Request $request, $id){
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository(User::class);
        $admin = $repository->findOneById($id);

        if($admin == null){
            throw new NotFoundHttpException('No se encontró el usuario que estas buscando.');
        }

        if($admin->getAdministrador() == null || !in_array('ROLE_ADMIN', $admin->getRoles())){
            throw new NotFoundHttpException('El usuario que buscas no es un administrador.');
        }

        $admin->setEnabled(false);
        $admin->removeRole('ROLE_ADMIN');

        foreach ($admin->getAdministrador()->getAsesores() as $asesor) {
            if(count($asesor->getAdministradores()) == 1){
                $asesor->getUser()->removeRole('ROLE_ASESOR');
                $asesor->getUser()->addRole('ROLE_ALUMNO');

                $asesor->removeAdministradore($admin->getAdministrador());

                $em->persist($asesor);
                $em->persist($asesor->getUser());
            }
        }

        $admin->getAdministrador()->getAsesores()->clear();

        $em->persist($admin->getAdministrador());
        $em->persist($admin);
        $em->flush();

        return $this->redirectToRoute('administradores');
    }
}
