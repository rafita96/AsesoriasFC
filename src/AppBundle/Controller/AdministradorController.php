<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use AppBundle\Entity\User;
use AppBundle\Entity\Asesor;
use AppBundle\Entity\Alumno;
use AppBundle\Entity\Administrador;
use AppBundle\Entity\Cita;
use AppBundle\Entity\Token;

use AppBundle\Form\AdminType;
use AppBundle\Form\AsesorType;

/**
* Controlador de los administradores
*/
class AdministradorController extends Controller
{

    /**
     * @Route("/register/admin/{token}", name="register_admin")
     * 
     * Un administrador solamente puede ser registrado por un super administrador
     */
    public function registrarAdminAction(Request $request, $token){

        // Se supone que al nuevo administrador se le envía una liga de registro.
        // Esta liga tiene un token, entonces se busca en la base de datos si es un token valido.
        $repository = $this->getDoctrine()->getRepository(Token::class);
        $ttoken = $repository->findOneByToken($token);

        // Si no existe, entonces no lo dejamos seguir.
        if($ttoken == null){
            $this->addFlash('danger','Token inválido.');
            return $this->redirectToRoute('homepage');
        }

        // Como si existe, entonces buscamos el usuario por el correo relacionado con ese token.
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->findOneByUsername($ttoken->getCorreo());

        $em = $this->getDoctrine()->getManager();
        // Si el usuario existe significa que ya estaba registrado.
        if($user != null){

            // Se verifica que el usuario no sea administrador o super administrador
            if(in_array("ROLE_ADMIN", $user->getRoles()) || in_array('ROLE_SUPER_ADMIN', $user->getRoles())){
                // Si ya es administrador o super administrador, entonces no hay que hacer algo.
                $this->addFlash('danger','Ya eras administrador.');
            }else{

                // Como es un usuario registrado, entonces hay que quitarle los roles asignados...
                $user->removeRole("ROLE_ASESOR");
                $user->removeRole("ROLE_ALUMNO");
                // y ponerle el de administrador
                $user->addRole("ROLE_ADMIN");

                // Como un administrador, también es un asesor.
                // Entonces verificamos si el usuario ya era asesor. 
                if($user->getAsesor() == null){
                    // Si no era asesor, entonces le creamos uno.
                    $asesor = new Asesor();
                    $asesor->setUser($user);
                    $user->setAsesor($asesor);
                    // y lo guardamos en la base de datos
                    $em->persist($asesor);
                }

                // Puede que este usuario haya sido administrador antes, y que su usuario
                // tenga un administrador relacionado.
                if($user->getAdministrador() == null){
                    // Si no tiene un administrador relacionado, entonces le creamos uno
                    $administrador = new Administrador();
                    $administrador->setUser($user);
                    $user->setAdministrador($administrador);
                    // y lo guardamos en la base de datos
                    $em->persist($administrador);
                }
                // Se actualiza la información del usuario
                $em->persist($user);
                // y eliminamos el token de la base de datos.
                $em->remove($ttoken);
                $em->flush();

                // Ahora es un administrador
                $this->addFlash('success','Ahora eres un administrador.');
            }

            // Se redirecciona a su página principal.
            return $this->redirectToRoute('homepage');
        }

        // En este caso, el usuario no estaba registrado
        //  Así que se debe crear uno nuevo
        $user = new User();
        // Se utiliza el formulario de AdminType
        $form = $this->createForm(AdminType::class, $user);

        $form->handleRequest($request);
        // Si el formulario fue enviado, entonces continuamos con el registro.
        if ($form->isSubmitted() && $form->isValid()){
            
            // Se obtiene el correo del token.
            // En ese correo no se encuentra la parte del dominio, @uabc.edu.mx
            $correo = $ttoken->getCorreo();
            // Suponiendo que este sistema es solo para usuarios de uabc
            // entonces se asigna el correo al nuevo usuario.
            $user->setEmail($correo.'@uabc.edu.mx');
            $user->setUsername($correo);
            $user->setEnabled(true);

            // Codifica la contraseña introducida en el campo del formulario.
            $encoder = $this->container->get("security.password_encoder");
            $encoded = $encoder->encodePassword($user, $form["password"]->getData());
            $user->setPassword($encoded);

            // Se le agrega el rol de administrador
            $user->addRole('ROLE_ADMIN');

            // Se crea un administrador relacionado con el usuario
            $administrador = new Administrador();
            $administrador->setUser($user);
            $user->setAdministrador($administrador);

            // y se crea un asesor relacionado al usuario.
            $asesor = new Asesor();
            $asesor->setUser($user);
            $user->setAsesor($asesor);
            $em->persist($asesor);

            // Se guarda la nueva información.
            $em->persist($user);
            $em->persist($administrador);
            // y se elimina el token.
            $em->remove($ttoken);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        // Como el formulario no ha sido enviado, entonces se muestra la página de registro
        // para administradores
        return $this->render('admin/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/", name="admin_home")
     *
     * Página principal para administradores
     */
    public function homeAction(){
        // Por defecto, el firewall asegura que el que entra aquí es un administrador

        // Entonces se debe verificar si es un super administrador o uno normal
        $user = $this->getUser();
        $roles = $user->getRoles();
        if(in_array("ROLE_SUPER_ADMIN", $roles))
        {
            $admin = $user;
        }else{
            $admin = null;
        }

        // Se obtienen los asesores del administrador
        $asesores = $user->getAdministrador()->getAsesores();

        // Y se muestran en la página principal.
        return $this->render('admin/home.html.twig', array('asesores' => $asesores, 'admin' => $admin));
    }

    /**
     * @Route("/admin/add/", name="admin_add")
     *
     * Agrega un asesor a la lista de asesores del administrador
     */
    public function addAction(Request $request, \Swift_Mailer $mailer){
        // Se obtiene el usuario actual
        $user = $this->getUser();
        $roles = $user->getRoles();
        $em = $this->getDoctrine()->getManager();

        // Solamente un administrador puede agregar asesores, entonces se debe 
        // verificar si el usuario actual es administrador
        if(in_array("ROLE_ADMIN", $roles) || in_array("ROLE_SUPER_ADMIN", $roles)){
            // Se recibe el correo escrito en el formulario.
            // Este correo corresponde al correo del alumno al que se quiere agregar
            // como asesor
            $correo = $request->request->get('correo');

            $repository = $em->getRepository(User::class);
            // Se busca un usuario registrado con ese correo
            $alumno = $repository->findOneByUsername($correo);

            // SI el usuario no está registrado, entonces...
            if($alumno == null){

                $repository = $this->getDoctrine()->getRepository(Token::class);
                // Si anteriormente se habia agregado al alumno como asesor,
                // pero el alumno no se registró es posible que el token correspondiente
                // a ese alumno siga existiendo.
                // Entonces se busca el token con ese correo
                $token = $repository->findOneByCorreo($correo);
                if($token == null){
                    // si no existe el token, entonces se crea uno nuevo
                    $token = new Token();
                    // relacionado al solicitante
                    $token->setUser($this->getUser());
                    $random = md5(random_bytes(10));
                    $token->setToken($random);
                    // y el correo al cual se quiere registrar
                    $token->setCorreo($correo);

                    // por último se guarda el token.
                    $em->persist($token);
                    $em->flush();
                }

                // Se crea un mensaje con la liga de registro con el token
                // correspondiente al nuevo asesor
                $message = (new \Swift_Message('Correo de confirmación'))
                    ->setFrom('asesoriasfc@noreply')
                    ->setTo($correo.'@uabc.edu.mx')
                    ->setBody(
                        $this->renderView(
                            // app/Resources/views/Emails/registration.html.twig
                            'email/registration_asesor.email.twig',
                            array(
                                'token' => $token->getToken(),
                                'admin' => $user)
                        ),
                        'text/html'
                    );

                // se envía el correo
                $mailer->send($message);

                // El usuario no es registrado directamente, sino que el asesor tendrá que registrarse 
                // por medio de esa liga.
                $this->addFlash('danger','El usuario aún no está registrado en el sistema, sin embargo se le envió un correo para que se registre. Una vez registrado se convertirá en asesor.');
                return $this->redirectToRoute('admin_home');
            }else{
                // En el caso que el usuario ya este registrado en el sistema
                // se verifica que no sea un administrador o super administrador
                if(in_array('ROLE_ADMIN', $alumno->getRoles()) || in_array('ROLE_SUPER_ADMIN', $alumno->getRoles())){
                    $this->addFlash('danger','No puedes agregar a un administrador como asesor.');
                    return $this->redirectToRoute('admin_home');
                }else if(in_array('ROLE_ALUMNO', $alumno->getRoles())){
                    // Si ya está registrado con el rol de alumno, 
                    // entonces se le quita ese rol
                    $alumno->removeRole('ROLE_ALUMNO');
                }

                // Si el usuario ya es asesor del administrador que quiere solicitarlo
                // entonces se redirige a la página principal del administrador
                foreach ($user->getAdministrador()->getAsesores() as $asesor) {
                    if($asesor->getUser() == $alumno){
                        $this->addFlash('danger','Ese usuario ya es tu asesor.');
                        return $this->redirectToRoute('admin_home'); 
                    }
                }

                // Puede que el alumno ya sea asesor, y no sea necesario agregarle el rol
                // pero si no es asesor...
                if(!in_array('ROLE_ASESOR', $alumno->getRoles())){
                    // entonces se le agrega el rol
                    $alumno->addRole('ROLE_ASESOR');
                }

                // Se verifica que el usuario tenga un asesor relacionado
                if($alumno->getAsesor() == null){
                    // si no lo tiene, entonces se crea uno
                    $asesor = new Asesor();
                    $asesor->setUser($alumno);
                    $em->persist($asesor);
                }else{
                    // sino entonces se utiliza el asesor relacionado
                    $asesor = $alumno->getAsesor();
                }
                
                // ahora se agrega el administrador a la lista de administradores
                // del asesor
                $asesor->addAdministradore($user->getAdministrador());
                // y por parte del administrador, se agrega el asesor a la lista
                // de asesores
                $user->getAdministrador()->addAsesore($asesor);

                // Por último se guarda todo en la base de datos.
                $em->persist($user);
                $em->persist($alumno);
                $em->persist($user->getAdministrador());
                $em->flush();

                // Todo está listo.
                $this->addFlash('success','El asesor fue agregado correctamente.');
                return $this->redirectToRoute('admin_home');
            }
        }

        // solo los administradores pueden agregar un asesor
        throw new AccessDeniedHttpException('No eres un administrador.');
    }

    /**
     * @Route("/admin/remove/{id}", defaults={"id" = -1}, name="admin_remove")
     */
    public function removeAction($id){

        $user = $this->getUser();
        $repository = $this->getDoctrine()->getRepository(Asesor::class);
        $alumno = $repository->findOneById($id);

        $user->getAdministrador()->removeAsesore($alumno);
        $alumno->removeAdministradore($user->getAdministrador());

        $alumno->getUser()->removeRole('ROLE_ASESOR');
        $alumno->getUser()->addRole('ROLE_ALUMNO');

        $em = $this->getDoctrine()->getEntityManager();
        $em->persist($user);
        $em->persist($alumno);
        $em->persist($user->getAdministrador());
        $em->persist($alumno->getUser());
        $em->flush();


        $this->addFlash('success','El asesor fue eliminado correctamente.');
        return $this->redirectToRoute('admin_home');
    }

    /**
     * @Route("/admin/asesor/{correo}", name="admin_asesor")
     */
    public function asesorAction($correo){
        $user = $this->getUser();

        // $em = $this->getDoctrine()->getEntityManager();
        $repository = $this->getDoctrine()->getEntityManager()->getRepository(User::class);
        $alumno = $repository->findOneByUsername($correo);
        // Si no existe el alumno
        if(is_null($alumno)){
            $this->addFlash('danger','No se pudo encontrar al asesor solicitado.');
            return $this->redirectToRoute('admin_home');                        
        }else{
            $roles = $user->getRoles();
            if(in_array("ROLE_SUPER_ADMIN", $roles)){
                $citas_gen = $this->getDoctrine()
                            ->getEntityManager()
                            ->getRepository(Cita::class)->findAll();
                $citas = array();
                $nombres = array();
                foreach ($citas_gen as &$cita){
                    if($cita->getAsesor() != null && $cita->getAsesor()->getId() == $alumno->getAsesor()->getId()){
                        array_push($citas, $cita);
                        $asesorado = $cita->getAlumno()->getUser();
                        $nombre = $asesorado->getNombre()." ".$asesorado->getAPaterno()." ".$asesorado->getAMaterno();
                        array_push($nombres, $nombre);
                    }
                }
                return $this->render('admin/asesor.html.twig', array('citas' => $citas, 'asesor' => $alumno->getAsesor(), 'nombres' => $nombres));
            }
            $asesores = $user->getAdministrador()->getAsesores();
            foreach ($asesores as &$asesor) {            
                if($asesor == $alumno->getAsesor()){
                    $citas_gen = $this->getDoctrine()
                                ->getEntityManager()
                                ->getRepository(Cita::class)->findAll();
                    $citas = array();
                    $nombres = array();
                    foreach ($citas_gen as &$cita){
                        if($cita->getAsesor() != null && $cita->getAsesor()->getId() == $alumno->getAsesor()->getId()){
                            array_push($citas, $cita);
                            $asesorado = $cita->getAlumno()->getUser();
                            $nombre = $asesorado->getNombre()." ".$asesorado->getAPaterno()." ".$asesorado->getAMaterno();
                            array_push($nombres, $nombre);
                        }
                    }
                    return $this->render('admin/asesor.html.twig', array('citas' => $citas, 'asesor' => $asesor, 'nombres' => $nombres));
                }
            }

            $this->addFlash('danger','El asesor solicitado no esta asociado a su cuenta.');
            return $this->redirectToRoute('admin_home');
        }
    }


    /**
     * @Route("/register/asesor/{token}", name="register_asesor")
     */
    public function registrarAsesorAction(Request $request, $token){

        $repository = $this->getDoctrine()->getRepository(Token::class);
        $ttoken = $repository->findOneByToken($token);

        if($ttoken == null || $ttoken->getUser()->getAdministrador() == null){
            $this->addFlash('danger','Token inválido.');
            return $this->redirectToRoute('homepage');
        }

        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->findOneByUsername($ttoken->getCorreo());

        $em = $this->getDoctrine()->getManager();
        if($user != null){

            if(in_array("ROLE_ADMIN", $user->getRoles()) || in_array('ROLE_SUPER_ADMIN', $user->getRoles())){
                $this->addFlash('danger','No puedes ser asesor de otro administrador, porque eres administrador.');
            }else{

                $user->removeRole("ROLE_ALUMNO");
                $user->addRole("ROLE_ASESOR");

                if($user->getAlumno() == null){
                    $alumno = new Alumno();
                    $alumno->setMatricula(md5(random_bytes(10)));
                    $alumno->setUser($user);
                    $user->setAlumno($alumno);
                    $em->persist($asesor);
                }

                if($user->getAsesor() == null){
                    $asesor = new Asesor();
                    $asesor->setUser($user);
                    $user->setAsesor($asesor);
                }else{
                    $asesor = $user->getAsesor();
                }
                
                $administrador = $ttoken->getUser()->getAdministrador();

                $asesor->addAdministradore($administrador);
                $administrador->addAsesore($asesor);
                $em->persist($asesor);
                $em->persist($administrador);

                $em->persist($user);
                $em->remove($ttoken);
                $em->flush();

                $this->addFlash('success','Ahora eres un asesor.');
            }

            return $this->redirectToRoute('homepage');
        }

        $user = new User();
        $form = $this->createForm(AsesorType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            
            $correo = $ttoken->getCorreo();
            $user->setEmail($correo.'@uabc.edu.mx');
            $user->setUsername($correo);
            $user->setEnabled(true);

            $encoder = $this->container->get("security.password_encoder");
            $encoded = $encoder->encodePassword($user, $form["password"]->getData());
            $user->setPassword($encoded);

            $user->addRole('ROLE_ASESOR');

            $alumno = new Alumno();
            $alumno->setUser($user);
            $alumno->setMatricula($form['matricula']->getData());
            $user->setAlumno($alumno);
            $em->persist($alumno);

            $asesor = new Asesor();
            $asesor->setUser($user);
            $user->setAsesor($asesor);

            $administrador = $ttoken->getUser()->getAdministrador();
            $asesor->addAdministradore($administrador);
            $administrador->addAsesore($asesor);

            $em->persist($administrador);
            $em->persist($asesor);

            $em->persist($user);
            $em->persist($alumno);
            $em->remove($ttoken);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        
        return $this->render('admin/new_asesor.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}   
