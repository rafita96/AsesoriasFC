Asesorias FC
=========

Sistema web hecho con Symfony 2.8, que permite a un alumno hacer una solicitud para una asesoría.

#Iniciar el proyecto

Cuando se descarga el proyecto por primera vez, no viene con las dependencias. Por lo tanto es necesario descargarlas con [composer](https://getcomposer.org/):
```
composer update
```

Puede que esto falle, porque se modificó de forma incorrecta un comando de la dependencia FOSUserBundle. Así que es necesario copiar el encabezado de la función 'create' de la clase [UserManipulator](src/AppBundle/Tools/UserManipulator.php) que se encuentra en src/AppBundle/Tools/UserManipulator.php. Una vez copiado es necesario pegarlo en el encabezado de la función 'create' de la clase UserManipulator, que se debe encontrar en vendor/friendsofsymfony/user-bundle/Util/UserManipulator.php.

## Base de datos
Es necesario tener los servicios de apache y mysql activos.
En el archivo [parameters.yml](app/config/parameters.yml) se debe poner la dirección de la base de datos, el usuario y su contraseña.

Después se debe ejecutar el siguiente comando
```
php bin/console doctrine:database:create
```
para crear la base de datos.
Posteriormente se deben crear las tablas con el siguiente comando:
```
php bin/console doctrine:schema:update --force
```
Y para tener la base de datos lista con información predefinida y necesaria para el sistema, se debe ejecutar el siguiente comando:
```
php bin/console app:init
```

Con esto ya debe estar lista la base de datos.

## Super Administrador
El super administrador solamente se puede crear con el siguiente comando:
```
php bin/console fos:user:create
``` 